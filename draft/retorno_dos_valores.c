#include <stdio.h>

int sum_rest (int, int, int*);

int main()
{
  int a=10;
  int b=17;
  int resta=0;
  int suma=0;

  suma=sum_rest(a,b,&resta);

  printf("suma %d resta %d",suma,resta);
}

int sum_rest (int a_val,int b_val, int *resta_ptr)
{
  *resta_ptr=a_val-b_val;
   return(a_val+b_val);
}
