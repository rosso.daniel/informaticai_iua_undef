# informaticaI_IUA
Este repositorio contiene material de apoyo para la cátedra Informatica I de la carrera Ingeniería Informática, dictada en el
Centro Regional Universitario Córdoba - IUA - Universidad de la Defensa Nacional

Disclaimer
=======
Estas presentaciones tienen el objetivo de dar soporte al dictado de clases. De ninguna manera pueden sustituir los apuntes tomados en clases y/o la asistencia a las mismas. Por otro lado, todo el contenido de este repositorio se encuentra en versión alfa. Es decir, puede contener errores.

Contacto
=======
Daniel A. Rosso
drosso@iua.edu.ar



Project layout:
=======
```
├── dbases: almacena archivo json para autogenerar los slides a partir de los temas (TBD)
├── draft: información temporal y no importante
├── exported: archivos pdf exportados
│   ├── 2021: slides utilizados para el ciclo lectivo 2021
│   └── 2022: slides utilizados para el ciclo lectivo 2022
├── sanity_test: se utilizará para CI/CD de gitlab
├── scripting: scripts de python de soporte para la autogeneración y mantenimiento del material
├── templates: plantillas para los diferentes temas
│   ├── c: plantillas para los códigos en C
│   ├── tex: plantillas para las presentaciones beamer
│   │    ├── img: imagenes utilizadas en el template
│   │    ├── src: códigos latex con el template base
│   │    └── sty: archivos de estilos para latex
│   └── topics_tree: árbol de directorios base para todos los temas
│        ├── c: almacena código C
│        ├── img: almacena las imágenes
│        │    ├── svg: imágenes vectoreales en formato .svg
│        │    └── exported: imágenes exportadas en formato PNG para ser utilizadas por la presentación
│        └── pdf: beamer exportado a pdf
└── topics_tree: sub temas divididos atomicamente
```