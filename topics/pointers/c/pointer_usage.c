#include <stdio.h>
int main()
{  int num1=48;
   int *dir_num_1;

   /*uso de la variable de tipo puntero*/
   dir_num_1=&num1;

   printf("Valor de num1 %d\n",num1);
   printf("Posicion de memoria de num1 %X\n",&num1);
   printf("Valor de dir_num_1 %X\n",dir_num_1);
   printf("Posicion de memoria de num1 %X\n",&dir_num_1);

   return (0);
}