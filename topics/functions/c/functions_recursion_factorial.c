#include<stdio.h>
int factorial(int n);
int main() {
    int n=0,resultado;
    printf("Ingrese un numero positivo\n");
    scanf("%d",&n);
    resultado=factorial(n);
    printf("Factorial de %d = %d\n", n, resultado);
    return 0;
}



int factorial(int n) 
{
    if (n>=1)
        //Llamada recursiva
        return n*  factorial(n-1);
    else
        //Caso base
        return 1;
}